package classes;
import java.util.*;
import java.util.Map.Entry;
public class Etudiant {

	private Identite id;
	private Formation form;
	private HashMap<String, ArrayList<Float>> hm;
	
	public Etudiant(Identite i, Formation f) {
		id=i;
		form=f;
		hm=new HashMap<String, ArrayList<Float>>();
	}
	
	public Formation getFormation() {
		return form;
	}

	public Identite getIdentite() {
		return id;
	}

	public HashMap<String, ArrayList<Float>> getNotes() {
		return hm;
	}
	
	public String toString() {
		String res = ("Identite : "+id.getNip()+" "+id.getNom()+" "+id.getPrenom()+" /Formation :" +form.getId());
		Iterator<String> i = this.hm.keySet().iterator(); 
		while(i.hasNext()) {
			String r = i.next();
			res+="matiere"+r;
			Iterator<Float> f = this.hm.get(i).iterator();
			while(f.hasNext()) {
				res+=f.next();
			}
		}
		return res;
		
	}
	
	public void ajouterNote(String matiere, float note) {
		if(note<=20 && note>=0) {
			if(this.hm.containsKey(matiere)) {
				this.hm.get(matiere).add(note);
			}else {
				Iterator<String> m = form.getMatiere().keySet().iterator();
				while(m.hasNext()) {
					if(m.next().equals(matiere)) {
						ArrayList<Float> n = new ArrayList<Float>();
						n.add(note);
						this.hm.put(matiere, n);
					}
				}
			}
		}

	}
	
	public void ajouterListeNote(String matiere, ArrayList<Float> lf) {
		boolean note=true;
		Iterator<Float> n = lf.iterator();
		while(n.hasNext()) {
			Float f = n.next();
			if(!(f<=20 && f>=0)) {
				note=false;
			}
		}
		if(note) {
			this.hm.put(matiere, lf);
		}
	}
	
	public float calculerMoy(String matiere) {
		float moy=0;
		int i=0;
		if(this.hm.containsKey(matiere)) {
			Iterator<Float> notes = this.hm.get(matiere).iterator();
			while(notes.hasNext()) {
				moy+=notes.next();
				i++;
			}
		}
		if(i!=0) {
			return moy/i;
		}else {
			return 0;
		}
	}
	
	public float calculerMoyGen() {
		float moygen=0;
		int i =0;
		HashMap<String,Integer> hash= this.form.getMatiere();
		Iterator<String> matiereF = hash.keySet().iterator();
		Iterator<String> matiereE = this.hm.keySet().iterator();
		while(matiereF.hasNext()) {
			while(matiereE.hasNext()) {
				String mate=matiereE.next();
				String matf=matiereF.next();
				if(matf.equals(mate)) {
					moygen+=calculerMoy(mate)*(hash.get(matf));
					i+=hash.get(matf);
				}
			}
		}
		if(i!=0) {
			return moygen/i;
		}else {
			return 0;
		}
	}
	
	
}
