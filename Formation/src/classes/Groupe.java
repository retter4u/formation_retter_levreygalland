package classes;
import java.util.ArrayList;
import java.util.List;

public class Groupe {
	
	private Formation formation;
	private List<Etudiant> etudiants;
	
	public Groupe(Formation formation, List<Etudiant> etudiants) {
		boolean memeFormation = true;
		for(Etudiant e : etudiants) {
			if(!e.getFormation().equals(formation)) {
				memeFormation = false;
			}
		}
		if(memeFormation) {
			this.formation = formation;
			this.etudiants = etudiants;
		}
	}
	
	public Formation getFormation() {
		return this.formation;
	}
	
	public List<Etudiant> getEtudiant(){
		return this.etudiants;
	}
	
	public void ajouterEtu(Etudiant etudiant) {
		boolean existedeja = false;
		if(etudiant==null) {
			existedeja=true;
		}
		for(Etudiant e : this.etudiants) {
			if(e.getIdentite().equals(etudiant.getIdentite()) && e.getFormation().getId().equals(etudiant.getFormation().getId())) {
				existedeja = true;
			}
		}
		if(!existedeja) {
			this.etudiants.add(etudiant);
		}
	}
	
	public void supprimerEtu(Etudiant etudiant) {
		boolean existepas = true;
		int i = 0;
		int index = 0;
		for(Etudiant e : this.etudiants) {
			if(e.equals(etudiant)) {
				index = i;
				existepas = false;
			}
			i++;
		}
		if(!existepas) {
			this.etudiants.remove(index);
		}
	}
	
	public float calculerMoy(String matiere) {
		float somme = 0;
		int i = 0;
		for(Etudiant e : this.etudiants) {
			somme += e.calculerMoy(matiere);
			i++;
		}
		float moy = somme/i;
		return moy;
	}
	
	public float calculerMoyGen() {
		float somme = 0;
		int i = 0;
		for(Etudiant e : this.etudiants) {
			somme += e.calculerMoyGen();
			i++;
		}
		float moy = somme/i;
		return moy;
	}
	
	public void triParMerite() {
		List<Etudiant> le = new ArrayList<Etudiant>();
		while(0 < this.etudiants.size()) {
			Etudiant etmp = this.etudiants.get(0);
			for(Etudiant e : this.etudiants) {
				if(e.calculerMoyGen() > etmp.calculerMoyGen()) {
					etmp = e;
				}
			}
			le.add(etmp);
			this.etudiants.remove(etmp);
		}
		this.etudiants = le;
	}
	
	public void triAlpha() {
		List<Etudiant> le = new ArrayList<Etudiant>();
		while(0 < this.etudiants.size()) {
			Etudiant etmp = this.etudiants.get(0);
			for(Etudiant e : this.etudiants) {
				if(e.getIdentite().getNom().compareTo(etmp.getIdentite().getNom()) < 0) {
					etmp = e;
				} else if(e.getIdentite().getNom().compareTo(etmp.getIdentite().getNom()) == 0) {
					if(e.getIdentite().getPrenom().compareTo(etmp.getIdentite().getPrenom()) < 0) {
						etmp=e;
					}
				}
			}
			le.add(etmp);
			this.etudiants.remove(etmp);
		}
		this.etudiants = le;
	}
}
