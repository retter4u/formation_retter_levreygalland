package classes;

public class Identite {

	private String nip, nom, prenom;
	
	public Identite(String n, String no, String p) {
		nip=n;
		nom=no;
		prenom=p;
	}
	
	public String getNip() {
		return nip;
	}
	
	public String getNom() {
		return nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public String toString() {
		return (nip+" "+nom+" "+prenom);
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result =1;
		result = prime*result+((nip==null) ? 0 : nip.hashCode());
		result = prime*result+((nom==null) ? 0 : nom.hashCode());
		result = prime*result+((prenom==null) ? 0 : prenom.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this==o)
			return true;
		if(o==null)
			return false;
		if(getClass()!=o.getClass())
			return false;
		Identite id = (Identite) o;
		if(nip.equals(id.getNip())){
			if(nom.equals(id.getNom())) {
				if(prenom.equals(id.getPrenom())) {
					return true;
				}
			}
		}
		return false;
	}
	
}
