package classes;
import java.util.HashMap;
import java.util.Set;

public class Formation {
	
	private String id;
	private HashMap<String,Integer> matieres;
	
	public Formation(String id, HashMap<String,Integer> matieres) {
		this.id = id;
		this.matieres = matieres;
	}
	
	public String getId() {
		return this.id;
	}
	
	public HashMap getMatiere() {
		return this.matieres;
	}
	
	public String toString() {
		String res;
		Set<String> clefs = this.matieres.keySet();
		res = "Identifiant : " + this.id + "\n" + "Mati�res :" + "\n";
		for(String s : clefs) {
			res = res + "- " + s;
		}
		return res;
	}
	
	public void ajouterMat(String mat, int coef) {
		boolean b = true;
		Set<String> clefs = this.matieres.keySet();
		for(String s : clefs) {
			if(s.equals(mat)) {
				b = false;
			}
		}
		if(coef < 0) {
			b = false;
		}
		if(b) {
			this.matieres.put(mat,coef);
		}
	}
	
	public void suppressionMat(String mat) {
		if(matieres.containsKey(mat)) {
			matieres.remove(mat);
		}
	}
	
	public int getCoeff(String mat) {
		if(matieres.containsKey(mat)) {
			return matieres.get(mat);
		} else {
			return 0;
		}
		
	}
}
