package test;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;

import classes.*;

public class TestGroupe {

	public Groupe g;
	public Formation f,f2;
	public List<Etudiant> le, le2;
	public Etudiant e,e2,e3,e4;
	@Before
	public void initialize() {
		HashMap<String,Integer> matieres = new HashMap<String,Integer>();
		HashMap<String,Integer> matieres2 = new HashMap<String,Integer>();
		matieres.put("BDD",2);
		matieres.put("WEB",1);
		matieres.put("PO",4);
		matieres.put("COO",3);
		matieres.put("IHM",2);
		matieres2.put("Compta",8);
		Identite i = new Identite("0001","LEVREYGALLAND","Donatien");
		Identite i2 = new Identite("0002","RETTER","Valentin");
		Identite i3 = new Identite("0003","ANONYME", "Personne");
		f = new Formation("DUT Informatique", matieres);
		f2 = new Formation("DUT GEA", matieres2);
		e = new Etudiant(i,f);
		e2 = new Etudiant(i2,f);
		e3 = new Etudiant(i3, f2);
		e4 = new Etudiant(i3, f);
		le = new ArrayList<Etudiant>();
		le2 = new ArrayList<Etudiant>();
		le.add(e);
		le.add(e2);
		le.add(e3);
		le2.add(e);
		le2.add(e2);
		le2.add(e4);
	}
		
	@Test
	public void testConstructeur() {
		g=new Groupe(f,le);
		assertNull(g.getEtudiant());
		g=new Groupe(f,le2);
		assertNotNull(g.getEtudiant());
	}
	
	@Test
	public void testAjoutEtu() {
		g=new Groupe(f,le2);
		boolean troisieme=false;
		boolean cinquieme=false;
		boolean sixieme=false;
		
		//g.ajouterEtu(e3);
		
		//Etudiant e5 = null;
		//g.ajouterEtu(e5);
		
		Identite i = new Identite("0006","IEN","Seb");
		Etudiant e6 = new Etudiant(i,f);
		g.ajouterEtu(e6);
		
		Iterator<Etudiant> ge = g.getEtudiant().iterator();
		while(ge.hasNext()) {
			Etudiant ge2=ge.next();
			//if(ge2.getIdentite().getNip().equals("0003"))
				//troisieme=true;
			if(ge2.getIdentite().getNip().equals("0006"))
				sixieme=true;
		}
		
		assertFalse(troisieme);
		assertEquals("etudiant non instancie pas dans la liste", 4, g.getEtudiant().size());
		assertTrue(sixieme);
		
	}
	
	@Test
	public void testMoy() {
		g=new Groupe(f,le2);
		ArrayList<Float> notes = new ArrayList<Float>();
		ArrayList<Float> notes2 = new ArrayList<Float>();
		notes.add((float) 10);
		notes.add((float) 20);
		notes.add((float) 0);
		notes.add((float) 15);
		notes2.add((float) 11);
		notes2.add((float) 4);
		notes2.add((float) 18);
		e.ajouterListeNote("WEB", notes);
		e2.ajouterListeNote("WEB", notes);
		e4.ajouterListeNote("WEB",notes2);
		Float res = g.calculerMoy("WEB");
		assertEquals("la moyenne est bonne",11.17,res,0.01);
	}
	
	@Test
	public void testMoyGen() {
		g=new Groupe(f,le2);
		ArrayList<Float> notes = new ArrayList<Float>();
		ArrayList<Float> notes2 = new ArrayList<Float>();
		ArrayList<Float> notes3 = new ArrayList<Float>();
		ArrayList<Float> notes4 = new ArrayList<Float>();
		notes.add((float) 10);
		notes.add((float) 20);
		notes.add((float) 0);
		notes.add((float) 15);
		notes2.add((float) 11);
		notes2.add((float) 4);
		notes2.add((float) 18);
		notes3.add((float) 17);
		notes3.add((float) 14);
		notes4.add((float) 8);
		notes4.add((float) 9);
		
		e.ajouterListeNote("WEB", notes);
		e.ajouterListeNote("PO", notes3);
		e2.ajouterListeNote("WEB", notes);
		e2.ajouterListeNote("PO",notes4);
		e4.ajouterListeNote("WEB",notes2);
		e4.ajouterListeNote("PO", notes4);
		Float res = g.calculerMoyGen();
		assertEquals("la moyenne generale est bonne",11.17,res,0.01);
	}
	
	@Test
	public void testtriAlpha() {
		g=new Groupe(f,le2);
		g.triAlpha();
		String nm = g.getEtudiant().get(0).getIdentite().getNom();
		String nm2 = g.getEtudiant().get(1).getIdentite().getNom();
		String nm3 = g.getEtudiant().get(2).getIdentite().getNom();
		assertEquals("le tri est fait", "ANONYME",nm);
		assertEquals("le tri est fait", "LEVREYGALLAND",nm2);
		assertEquals("le tri est fait", "RETTER",nm3);
	}
	
	@Test
	public void testtriMerite() {
		g=new Groupe(f,le2);
		e.ajouterNote("WEB", 10);
		e2.ajouterNote("WEB", 5);
		e4.ajouterNote("WEB", 2);
		
		g.triParMerite();
		String nm = g.getEtudiant().get(0).getIdentite().getNom();
		String nm2 = g.getEtudiant().get(1).getIdentite().getNom();
		String nm3 = g.getEtudiant().get(2).getIdentite().getNom();
		assertEquals("le tri est fait", "LEVREYGALLAND",nm);
		assertEquals("le tri est fait", "RETTER",nm2);
		assertEquals("le tri est fait", "ANONYME",nm3);
	}
	
		
	
	
	
	
}

	
	
