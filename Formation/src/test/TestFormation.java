package test;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;

import classes.*;

public class TestFormation {
	
	public Formation f;
	public HashMap<String,Integer> matieres;
	
	@Before
	public void initialize() {
		matieres = new HashMap<String,Integer>();
		matieres.put("BDD",2);
		matieres.put("WEB",1);
		matieres.put("PO",4);
		matieres.put("COO",3);
		matieres.put("IHM",2);
	}
	
	@Test
	public void testConstructeur() {
		//f = new Formation("DUT Informatic", null);
		//assertTrue(f.getMatiere().isEmpty());
		f = new Formation("DUT Informatique", matieres);
		assertFalse(f.getMatiere().isEmpty());
	}
	
	@Test
	public void testAjoutMat() {
		f.ajouterMat("Art pla", -1);
		f.ajouterMat("Compta", 1);
		
		boolean ajoutArt=false;
		boolean ajoutCompta=false;
		Iterator<String> matiere = f.getMatiere().keySet().iterator();
		while(matiere.hasNext()) {
			String m = matiere.next();
			if(m.equals("Art pla"))
				ajoutArt=true;
			if(m.equals("Compta"))
				ajoutCompta=true;
		}
		assertFalse(ajoutArt);
		assertTrue(ajoutCompta);
	}
	
	@Test
	public void testGettercoef() {
		int res = f.getCoeff("Art pla");
		int res2 = f.getCoeff("WEB");
		assertEquals("le coeff est de 0",res,0);
		assertEquals("le coeff est bon",res2,1);
	}

}
