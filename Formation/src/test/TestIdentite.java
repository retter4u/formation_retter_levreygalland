package test;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import classes.Etudiant;
import classes.Formation;
import classes.Identite;

public class TestIdentite {

	public Identite i;
	@Before
	public void initialize() {
		i = new Identite("0001","LEVREYGALLAND","Donatien");
	}
		
	@Test
	public void testConstructeur() {
		assertEquals("Mauvais num�ro d'identification", i.getNip(), "0001");
		assertEquals("Mauvais nom", i.getNom(), "LEVREYGALLAND");
		assertEquals("Mauvais pr�nom", i.getPrenom(), "Donatien");
	}
	
	@Test
	public void testToString() {
		assertEquals("Mauvaise m�thode toString()", i.toString(), "0001 LEVREYGALLAND Donatien");
	}
	
	@Test
	public void testEquals() {
		Identite bonneCopie = new Identite("0001","LEVREYGALLAND","Donatien");
		Identite mauvaiseCopie = new Identite("0002","RETTER","Valentin");
		Identite mauvaisNip = new Identite("0002","LEVREYGALLAND","Donatien");
		Identite mauvaisNom = new Identite("0001","RETTER","Donatien");
		Identite mauvaisPrenom = new Identite("0001","LEVREYGALLAND","Valentin");
		Identite mauvaisNipEtNom = new Identite("0002","RETTER","Donatien");
		Identite mauvaisNomEtPrenom = new Identite("0001","RETTER","Valentin");
		Identite mauvaisPrenomEtNip = new Identite("0002","LEVREYGALLAND","Valentin");
		assertEquals("Doit �tre �gale � i", i.equals(bonneCopie), true);
		assertEquals("Doit ne pas �tre �gale � i", i.equals(mauvaiseCopie), false);
		assertEquals("Doit ne pas �tre �gale � i", i.equals(mauvaisNip), false);
		assertEquals("Doit ne pas �tre �gale � i", i.equals(mauvaisNom), false);
		assertEquals("Doit ne pas �tre �gale � i", i.equals(mauvaisPrenom), false);
		assertEquals("Doit ne pas �tre �gale � i", i.equals(mauvaisNipEtNom), false);
		assertEquals("Doit ne pas �tre �gale � i", i.equals(mauvaisNomEtPrenom), false);
		assertEquals("Doit ne pas �tre �gale � i", i.equals(mauvaisPrenomEtNip), false);
	}
}
