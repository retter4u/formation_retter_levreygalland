package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import classes.Etudiant;
import classes.Formation;
import classes.Identite;

public class TestEtudiant {

	public Etudiant e;
	@Before
	public void initialize() {
		HashMap<String,Integer> matieres = new HashMap<String,Integer>();
		matieres.put("BDD",2);
		matieres.put("WEB",1);
		matieres.put("PO",4);
		matieres.put("COO",3);
		matieres.put("IHM",2);
		Identite i = new Identite("0001","LEVREYGALLAND","Donatien");
		Formation f = new Formation("DUT Informatique", matieres);
		e = new Etudiant(i,f);
	}
		
	@Test
	public void testConstructeur() {
		assertEquals("Mauvais num�ro d'identit�", e.getIdentite().getNip(), "0001");
		assertEquals("Mauvais nom", e.getIdentite().getNom(), "LEVREYGALLAND");
		assertEquals("Mauvais pr�nom", e.getIdentite().getPrenom(), "Donatien");
		assertEquals("Mauvais identifiant de formation", e.getFormation().getId(), "DUT Informatique");
		assertEquals("Mauvais coefficient pour la mati�re BDD", e.getFormation().getMatiere().get("BDD"), 2);
		assertEquals("Mauvais coefficient pour la mati�re WEB", e.getFormation().getMatiere().get("WEB"), 1);
		assertEquals("Mauvais coefficient pour la mati�re PO", e.getFormation().getMatiere().get("PO"), 4);
		assertEquals("Mauvais coefficient pour la mati�re COO", e.getFormation().getMatiere().get("COO"), 3);
		assertEquals("Mauvais coefficient pour la mati�re IHM", e.getFormation().getMatiere().get("IHM"), 2);
	}
	
	@Test
	public void testToString() {
		//Voir s'il faut mettre la liste des mati�res de la formation dans le toString()
		assertEquals("Probl�me dans la m�thode toString", e.toString(), "Identite : 0001 LEVREYGALLAND Donatien /Formation :DUT Informatique");
	}
	
	@Test
	public void testAjouterNote() {
		e.ajouterNote("BDD", 10);
		e.ajouterNote("BDD", -3);
		e.ajouterNote("BDD", 0);
		e.ajouterNote("BDD", 25);
		e.ajouterNote("BDD", 15);
		assertEquals("Note non ins�r�e ou pas � la bonne place", e.getNotes().get("BDD").get(0), 10.0, 0.01);
		assertEquals("Note non ins�r�e ou pas � la bonne place", e.getNotes().get("BDD").get(1), 0.0, 0.01);
		assertEquals("Note non ins�r�e ou pas � la bonne place", e.getNotes().get("BDD").get(2), 15.0, 0.01);
	}
	
	@Test
	public void testAjouterListeNote() {
		ArrayList<Float> notes = new ArrayList<Float>();
		ArrayList<Float> notes2 = new ArrayList<Float>();
		notes.add((float) 10);
		notes.add((float) 20);
		notes.add((float) 25);
		notes.add((float) 0);
		notes.add((float) 15);
		notes2.add((float) 11);
		notes2.add((float) 4);
		notes2.add((float) 18);
		e.ajouterListeNote("BDD", notes);
		e.ajouterListeNote("BDD", notes2);
		assertEquals("Note non ins�r�e", e.getNotes().get("BDD").get(0), 11, 0.01);
		assertEquals("Note non ins�r�e", e.getNotes().get("BDD").get(1), 4, 0.01);
		assertEquals("Note non ins�r�e", e.getNotes().get("BDD").get(2), 18, 0.01);
	}
	
	@Test
	public void testCalculerMoy() {
		ArrayList<Float> notes = new ArrayList<Float>();
		notes.add((float) 19);
		notes.add((float) 17);
		notes.add((float) 8);
		notes.add((float) 4);
		notes.add((float) 12);
		e.ajouterListeNote("BDD", notes);
		assertEquals("Mauvaise moyenne", e.calculerMoy("BDD"), 12.0, 0.01);
		assertEquals("La matiere doit etre inexistante", e.calculerMoy("Inexistante"), 0.0, 0.01);
		assertEquals("La matiere n'a pas de notes", e.calculerMoy("WEB"), 0.0, 0.01);
	}
	
	@Test
	public void testCalculerMoyGen() {
		e.ajouterNote("BDD", 19);
		e.ajouterNote("WEB", 17);
		e.ajouterNote("PO", 8);
		e.ajouterNote("COO", 4);
		e.ajouterNote("IHM", 12);
		assertEquals("Mauvaise moyenne", e.calculerMoyGen(), 10.25, 0.01);
	}

}
